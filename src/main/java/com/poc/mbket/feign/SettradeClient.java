package com.poc.mbket.feign;

import com.poc.mbket.feign.config.FeignProxyClientConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "SettradeClient", configuration = FeignProxyClientConfig.class, url = "https://open-api-test.settrade.com/")
public interface SettradeClient {
    @GetMapping(path = "/open-api/document", produces = MediaType.APPLICATION_JSON_VALUE)
    String document();
}

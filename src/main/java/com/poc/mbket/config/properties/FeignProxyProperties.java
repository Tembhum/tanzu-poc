package com.poc.mbket.config.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "mbket.feign.proxy")
@Getter
@Setter
public class FeignProxyProperties {
    public static final String HOST_SEPARATOR = ";";
    private String host;
    private Integer port;
    private String nonProxyHosts;
}
